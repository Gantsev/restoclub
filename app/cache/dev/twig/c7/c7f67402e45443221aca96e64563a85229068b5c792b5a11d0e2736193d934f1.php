<?php

/* base.html.twig */

class __TwigTemplate_fed433b4597e75888d6c8889fcfbc174b579b3972301f7bbb0c7559b03cd9709 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'title'       => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'body'        => [$this, 'block_body'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_609871d47e9af27ef01fac73ef79ab3226382a330f3cf38fea054692e9d41f65 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_609871d47e9af27ef01fac73ef79ab3226382a330f3cf38fea054692e9d41f65->enter($__internal_609871d47e9af27ef01fac73ef79ab3226382a330f3cf38fea054692e9d41f65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 10
        $this->displayBlock('body', $context, $blocks);
        // line 11
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 12
        echo "    </body>
</html>
";

        $__internal_609871d47e9af27ef01fac73ef79ab3226382a330f3cf38fea054692e9d41f65->leave($__internal_609871d47e9af27ef01fac73ef79ab3226382a330f3cf38fea054692e9d41f65_prof);

    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $__internal_45ebdaf2bb6a86d22ba217be245a2c5dff99f1525e6ddfa2f3cdfbe9f8468002 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_45ebdaf2bb6a86d22ba217be245a2c5dff99f1525e6ddfa2f3cdfbe9f8468002->enter($__internal_45ebdaf2bb6a86d22ba217be245a2c5dff99f1525e6ddfa2f3cdfbe9f8468002_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";

        $__internal_45ebdaf2bb6a86d22ba217be245a2c5dff99f1525e6ddfa2f3cdfbe9f8468002->leave($__internal_45ebdaf2bb6a86d22ba217be245a2c5dff99f1525e6ddfa2f3cdfbe9f8468002_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = [])
    {
        $__internal_78c80b02b90d3761e107e66b2f8f79a3fb74e48552237949784933dd7099962e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78c80b02b90d3761e107e66b2f8f79a3fb74e48552237949784933dd7099962e->enter($__internal_78c80b02b90d3761e107e66b2f8f79a3fb74e48552237949784933dd7099962e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));


        $__internal_78c80b02b90d3761e107e66b2f8f79a3fb74e48552237949784933dd7099962e->leave($__internal_78c80b02b90d3761e107e66b2f8f79a3fb74e48552237949784933dd7099962e_prof);

    }

    // line 10
    public function block_body($context, array $blocks = [])
    {
        $__internal_53e083779ebd2c78ade82f2554ecde94b8ee71f96746ac624d4d9ac6a2c8f249 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_53e083779ebd2c78ade82f2554ecde94b8ee71f96746ac624d4d9ac6a2c8f249->enter($__internal_53e083779ebd2c78ade82f2554ecde94b8ee71f96746ac624d4d9ac6a2c8f249_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));


        $__internal_53e083779ebd2c78ade82f2554ecde94b8ee71f96746ac624d4d9ac6a2c8f249->leave($__internal_53e083779ebd2c78ade82f2554ecde94b8ee71f96746ac624d4d9ac6a2c8f249_prof);

    }

    // line 11
    public function block_javascripts($context, array $blocks = [])
    {
        $__internal_8c185547c011ec5c30219b0cd831982aa3e3bbf7619170383f1360d06b0742b3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c185547c011ec5c30219b0cd831982aa3e3bbf7619170383f1360d06b0742b3->enter($__internal_8c185547c011ec5c30219b0cd831982aa3e3bbf7619170383f1360d06b0742b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));


        $__internal_8c185547c011ec5c30219b0cd831982aa3e3bbf7619170383f1360d06b0742b3->leave($__internal_8c185547c011ec5c30219b0cd831982aa3e3bbf7619170383f1360d06b0742b3_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return [93 => 11, 82 => 10, 71 => 6, 59 => 5, 50 => 12, 47 => 11, 45 => 10, 38 => 7, 36 => 6, 32 => 5, 26 => 1,];
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}{% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
        {% block body %}{% endblock %}
        {% block javascripts %}{% endblock %}
    </body>
</html>
", "base.html.twig", "/home/serg/restoclub/app/Resources/views/base.html.twig");
    }
}
