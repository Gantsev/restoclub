<?php

/* article/index.html.twig */

class __TwigTemplate_2e2f6502343d1067e2c1709b26fd268ad105e3182ce1e3018f9dc8216e13113e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "article/index.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_dcab8078054ddcb6ab9259eb4d3fdc04ca7a962abbd9d4a9d7843f2b5511544a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dcab8078054ddcb6ab9259eb4d3fdc04ca7a962abbd9d4a9d7843f2b5511544a->enter($__internal_dcab8078054ddcb6ab9259eb4d3fdc04ca7a962abbd9d4a9d7843f2b5511544a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "article/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));

        $__internal_dcab8078054ddcb6ab9259eb4d3fdc04ca7a962abbd9d4a9d7843f2b5511544a->leave($__internal_dcab8078054ddcb6ab9259eb4d3fdc04ca7a962abbd9d4a9d7843f2b5511544a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_38ffaf543ef4dad03ffbe23ca1fe46023e572e4f66420d197f91de1188d306de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_38ffaf543ef4dad03ffbe23ca1fe46023e572e4f66420d197f91de1188d306de->enter($__internal_38ffaf543ef4dad03ffbe23ca1fe46023e572e4f66420d197f91de1188d306de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Articles list</h1>
    <table>
        <thead>
            <tr>
                <th><a href=\"";
        // line 8
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articletagged_index");
        echo "\">Standard articles</a></th>
            </tr>
            <tr>
                <th><a href=\"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articleauthors_index");
        echo "\">Authors articles</a></th>
            </tr>
        </thead>
    </table>
";

        $__internal_38ffaf543ef4dad03ffbe23ca1fe46023e572e4f66420d197f91de1188d306de->leave($__internal_38ffaf543ef4dad03ffbe23ca1fe46023e572e4f66420d197f91de1188d306de_prof);

    }

    public function getTemplateName()
    {
        return "article/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return [52 => 11, 46 => 8, 40 => 4, 34 => 3, 11 => 1,];
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Articles list</h1>
    <table>
        <thead>
            <tr>
                <th><a href=\"{{ path('articletagged_index') }}\">Standard articles</a></th>
            </tr>
            <tr>
                <th><a href=\"{{ path('articleauthors_index') }}\">Authors articles</a></th>
            </tr>
        </thead>
    </table>
{% endblock %}
", "article/index.html.twig", "/home/serg/restoclub/app/Resources/views/article/index.html.twig");
    }
}
