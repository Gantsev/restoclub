<?php

/* articleauthors/show.html.twig */

class __TwigTemplate_83a87803d499d3b74e1b6e5564cebda9a285ac754c71436ca7e9231c60adf80f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "articleauthors/show.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_49f3452fa6667a367b639743c4660bb0f197dd58727fceb2abcc71c780a1c3a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_49f3452fa6667a367b639743c4660bb0f197dd58727fceb2abcc71c780a1c3a3->enter($__internal_49f3452fa6667a367b639743c4660bb0f197dd58727fceb2abcc71c780a1c3a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "articleauthors/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));

        $__internal_49f3452fa6667a367b639743c4660bb0f197dd58727fceb2abcc71c780a1c3a3->leave($__internal_49f3452fa6667a367b639743c4660bb0f197dd58727fceb2abcc71c780a1c3a3_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_ef92cab3b97086edb799398a0e07e9999d1abf5cf4cafe14e55625d7c09b6d2b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ef92cab3b97086edb799398a0e07e9999d1abf5cf4cafe14e55625d7c09b6d2b->enter($__internal_ef92cab3b97086edb799398a0e07e9999d1abf5cf4cafe14e55625d7c09b6d2b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Articleauthor</h1>

    <table>
        <tbody>
            <tr>
                <th>Author</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleAuthor"]) || array_key_exists("articleAuthor", $context) ? $context["articleAuthor"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleAuthor" does not exist.', 10, $this->getSourceContext());
        })()), "author", []), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Site</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleAuthor"]) || array_key_exists("articleAuthor", $context) ? $context["articleAuthor"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleAuthor" does not exist.', 14, $this->getSourceContext());
        })()), "site", []), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Id</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleAuthor"]) || array_key_exists("articleAuthor", $context) ? $context["articleAuthor"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleAuthor" does not exist.', 18, $this->getSourceContext());
        })()), "id", []), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Title</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleAuthor"]) || array_key_exists("articleAuthor", $context) ? $context["articleAuthor"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleAuthor" does not exist.', 22, $this->getSourceContext());
        })()), "title", []), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Url</th>
                <td>";
        // line 26
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleAuthor"]) || array_key_exists("articleAuthor", $context) ? $context["articleAuthor"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleAuthor" does not exist.', 26, $this->getSourceContext());
        })()), "url", []), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Text</th>
                <td>";
        // line 30
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleAuthor"]) || array_key_exists("articleAuthor", $context) ? $context["articleAuthor"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleAuthor" does not exist.', 30, $this->getSourceContext());
        })()), "text", []), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Created</th>
                <td>";
        // line 34
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleAuthor"]) || array_key_exists("articleAuthor", $context) ? $context["articleAuthor"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleAuthor" does not exist.', 34, $this->getSourceContext());
        })()), "created", [])) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleAuthor"]) || array_key_exists("articleAuthor", $context) ? $context["articleAuthor"] : (function () {
                throw new Twig_Error_Runtime('Variable "articleAuthor" does not exist.', 34, $this->getSourceContext());
            })()), "created", []), "Y-m-d H:i:s"), "html", null, true);
        }
        echo "</td>
            </tr>
            <tr>
                <th>Published</th>
                <td>";
        // line 38
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleAuthor"]) || array_key_exists("articleAuthor", $context) ? $context["articleAuthor"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleAuthor" does not exist.', 38, $this->getSourceContext());
        })()), "published", [])) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleAuthor"]) || array_key_exists("articleAuthor", $context) ? $context["articleAuthor"] : (function () {
                throw new Twig_Error_Runtime('Variable "articleAuthor" does not exist.', 38, $this->getSourceContext());
            })()), "published", []), "Y-m-d H:i:s"), "html", null, true);
        }
        echo "</td>
            </tr>
            <tr>
                <th>Updated</th>
                <td>";
        // line 42
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleAuthor"]) || array_key_exists("articleAuthor", $context) ? $context["articleAuthor"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleAuthor" does not exist.', 42, $this->getSourceContext());
        })()), "updated", [])) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleAuthor"]) || array_key_exists("articleAuthor", $context) ? $context["articleAuthor"] : (function () {
                throw new Twig_Error_Runtime('Variable "articleAuthor" does not exist.', 42, $this->getSourceContext());
            })()), "updated", []), "Y-m-d H:i:s"), "html", null, true);
        }
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 49
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articleauthors_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articleauthors_edit", ["id" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleAuthor"]) || array_key_exists("articleAuthor", $context) ? $context["articleAuthor"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleAuthor" does not exist.', 52, $this->getSourceContext());
        })()), "id", [])]), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 55
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) || array_key_exists("delete_form", $context) ? $context["delete_form"] : (function () {
            throw new Twig_Error_Runtime('Variable "delete_form" does not exist.', 55, $this->getSourceContext());
        })()), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 57
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) || array_key_exists("delete_form", $context) ? $context["delete_form"] : (function () {
            throw new Twig_Error_Runtime('Variable "delete_form" does not exist.', 57, $this->getSourceContext());
        })()), 'form_end');
        echo "
        </li>
    </ul>
";

        $__internal_ef92cab3b97086edb799398a0e07e9999d1abf5cf4cafe14e55625d7c09b6d2b->leave($__internal_ef92cab3b97086edb799398a0e07e9999d1abf5cf4cafe14e55625d7c09b6d2b_prof);

    }

    public function getTemplateName()
    {
        return "articleauthors/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return [137 => 57, 132 => 55, 126 => 52, 120 => 49, 108 => 42, 99 => 38, 90 => 34, 83 => 30, 76 => 26, 69 => 22, 62 => 18, 55 => 14, 48 => 10, 40 => 4, 34 => 3, 11 => 1,];
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Articleauthor</h1>

    <table>
        <tbody>
            <tr>
                <th>Author</th>
                <td>{{ articleAuthor.author }}</td>
            </tr>
            <tr>
                <th>Site</th>
                <td>{{ articleAuthor.site }}</td>
            </tr>
            <tr>
                <th>Id</th>
                <td>{{ articleAuthor.id }}</td>
            </tr>
            <tr>
                <th>Title</th>
                <td>{{ articleAuthor.title }}</td>
            </tr>
            <tr>
                <th>Url</th>
                <td>{{ articleAuthor.url }}</td>
            </tr>
            <tr>
                <th>Text</th>
                <td>{{ articleAuthor.text }}</td>
            </tr>
            <tr>
                <th>Created</th>
                <td>{% if articleAuthor.created %}{{ articleAuthor.created|date('Y-m-d H:i:s') }}{% endif %}</td>
            </tr>
            <tr>
                <th>Published</th>
                <td>{% if articleAuthor.published %}{{ articleAuthor.published|date('Y-m-d H:i:s') }}{% endif %}</td>
            </tr>
            <tr>
                <th>Updated</th>
                <td>{% if articleAuthor.updated %}{{ articleAuthor.updated|date('Y-m-d H:i:s') }}{% endif %}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('articleauthors_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('articleauthors_edit', { 'id': articleAuthor.id }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "articleauthors/show.html.twig", "/home/serg/restoclub/app/Resources/views/articleauthors/show.html.twig");
    }
}
