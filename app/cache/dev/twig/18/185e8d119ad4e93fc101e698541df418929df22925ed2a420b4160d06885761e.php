<?php

/* articletagged/index.html.twig */

class __TwigTemplate_966a8942586b71ffac770f8862b76eaf69bdc87143f7df32aa16c8bb0636a4f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "articletagged/index.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_a8a028ce30b4e3cb4e97ba61bfb23999235084c84f3899a56ac6d271f2313137 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a8a028ce30b4e3cb4e97ba61bfb23999235084c84f3899a56ac6d271f2313137->enter($__internal_a8a028ce30b4e3cb4e97ba61bfb23999235084c84f3899a56ac6d271f2313137_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "articletagged/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));

        $__internal_a8a028ce30b4e3cb4e97ba61bfb23999235084c84f3899a56ac6d271f2313137->leave($__internal_a8a028ce30b4e3cb4e97ba61bfb23999235084c84f3899a56ac6d271f2313137_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_e008020d6d59e65a8a5889b5890676ea5f0bcbbd399c1efa58e852636fd2f862 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e008020d6d59e65a8a5889b5890676ea5f0bcbbd399c1efa58e852636fd2f862->enter($__internal_e008020d6d59e65a8a5889b5890676ea5f0bcbbd399c1efa58e852636fd2f862_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Articletaggeds list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Url</th>
                <th>Text</th>
                <th>Created</th>
                <th>Published</th>
                <th>Updated</th>
                <th>Tags</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 21
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["articleTaggeds"]) || array_key_exists("articleTaggeds", $context) ? $context["articleTaggeds"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleTaggeds" does not exist.', 21, $this->getSourceContext());
        })()));
        foreach ($context['_seq'] as $context["_key"] => $context["articleTagged"]) {
            // line 22
            echo "            <tr>
                <td><a href=\"";
            // line 23
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articletagged_show", ["url" => twig_get_attribute($this->env, $this->getSourceContext(), $context["articleTagged"], "url", [])]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["articleTagged"], "id", []), "html", null, true);
            echo "</a></td>
                <td>";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["articleTagged"], "title", []), "html", null, true);
            echo "</td>
                <td>";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["articleTagged"], "url", []), "html", null, true);
            echo "</td>
                <td>";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["articleTagged"], "text", []), "html", null, true);
            echo "</td>
                <td>";
            // line 27
            if (twig_get_attribute($this->env, $this->getSourceContext(), $context["articleTagged"], "created", [])) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["articleTagged"], "created", []), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>";
            // line 28
            if (twig_get_attribute($this->env, $this->getSourceContext(), $context["articleTagged"], "published", [])) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["articleTagged"], "published", []), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>";
            // line 29
            if (twig_get_attribute($this->env, $this->getSourceContext(), $context["articleTagged"], "updated", [])) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["articleTagged"], "updated", []), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>
                    ";
            // line 31
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), $context["articleTagged"], "tags", []));
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                // line 32
                echo "                        <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articletagged_tag_show", ["tag" => twig_get_attribute($this->env, $this->getSourceContext(), $context["tag"], "name", [])]), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["tag"], "name", []), "html", null, true);
                echo "</a>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 34
            echo "                </td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articletagged_show", ["url" => twig_get_attribute($this->env, $this->getSourceContext(), $context["articleTagged"], "url", [])]), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articletagged_edit", ["id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["articleTagged"], "id", [])]), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['articleTagged'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 52
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articletagged_new");
        echo "\">Create a new articleTagged</a>
        </li>
    </ul>
";

        $__internal_e008020d6d59e65a8a5889b5890676ea5f0bcbbd399c1efa58e852636fd2f862->leave($__internal_e008020d6d59e65a8a5889b5890676ea5f0bcbbd399c1efa58e852636fd2f862_prof);

    }

    public function getTemplateName()
    {
        return "articletagged/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return [149 => 52, 142 => 47, 130 => 41, 124 => 38, 118 => 34, 107 => 32, 103 => 31, 96 => 29, 90 => 28, 84 => 27, 80 => 26, 76 => 25, 72 => 24, 66 => 23, 63 => 22, 59 => 21, 40 => 4, 34 => 3, 11 => 1,];
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Articletaggeds list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Url</th>
                <th>Text</th>
                <th>Created</th>
                <th>Published</th>
                <th>Updated</th>
                <th>Tags</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for articleTagged in articleTaggeds %}
            <tr>
                <td><a href=\"{{ path('articletagged_show', { 'url': articleTagged.url }) }}\">{{ articleTagged.id }}</a></td>
                <td>{{ articleTagged.title }}</td>
                <td>{{ articleTagged.url }}</td>
                <td>{{ articleTagged.text }}</td>
                <td>{% if articleTagged.created %}{{ articleTagged.created|date('Y-m-d H:i:s') }}{% endif %}</td>
                <td>{% if articleTagged.published %}{{ articleTagged.published|date('Y-m-d H:i:s') }}{% endif %}</td>
                <td>{% if articleTagged.updated %}{{ articleTagged.updated|date('Y-m-d H:i:s') }}{% endif %}</td>
                <td>
                    {% for tag in articleTagged.tags %}
                        <a href=\"{{ path('articletagged_tag_show', {'tag' : tag.name}) }}\">{{ tag.name }}</a>
                    {% endfor %}
                </td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('articletagged_show', { 'url': articleTagged.url }) }}\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('articletagged_edit', { 'id': articleTagged.id }) }}\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('articletagged_new') }}\">Create a new articleTagged</a>
        </li>
    </ul>
{% endblock %}
", "articletagged/index.html.twig", "/home/serg/restoclub/app/Resources/views/articletagged/index.html.twig");
    }
}
