<?php

/* @WebProfiler/Profiler/ajax_layout.html.twig */
class __TwigTemplate_abe4c6d67be0cf387bee4d5bb1bf1285b5d81195a5bbee9f51aca12378392d59 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'panel' => [$this, 'block_panel'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_44e501b45eabdc856178acb98bfef1e1be662c3684275f2dd89a106c514b20e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_44e501b45eabdc856178acb98bfef1e1be662c3684275f2dd89a106c514b20e7->enter($__internal_44e501b45eabdc856178acb98bfef1e1be662c3684275f2dd89a106c514b20e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);

        $__internal_44e501b45eabdc856178acb98bfef1e1be662c3684275f2dd89a106c514b20e7->leave($__internal_44e501b45eabdc856178acb98bfef1e1be662c3684275f2dd89a106c514b20e7_prof);

    }

    public function block_panel($context, array $blocks = [])
    {
        $__internal_797a3eb5b8f08ed50d70e448ce33d2ec40221d14b47ac03a9a7e387e555850e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_797a3eb5b8f08ed50d70e448ce33d2ec40221d14b47ac03a9a7e387e555850e9->enter($__internal_797a3eb5b8f08ed50d70e448ce33d2ec40221d14b47ac03a9a7e387e555850e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";

        $__internal_797a3eb5b8f08ed50d70e448ce33d2ec40221d14b47ac03a9a7e387e555850e9->leave($__internal_797a3eb5b8f08ed50d70e448ce33d2ec40221d14b47ac03a9a7e387e555850e9_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return [23 => 1,];
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "@WebProfiler/Profiler/ajax_layout.html.twig", "/home/serg/restoclub/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
