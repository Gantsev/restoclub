<?php

/* articleauthors/new.html.twig */

class __TwigTemplate_ebd2764218ab3f5f5011c5345b6a9bfa2878ea1909e435be284dc6fb244bebcd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "articleauthors/new.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_7d46122e1e03c0c9b022f55fae25c1cbe29f6b7acb7d4bde38290734ceece348 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d46122e1e03c0c9b022f55fae25c1cbe29f6b7acb7d4bde38290734ceece348->enter($__internal_7d46122e1e03c0c9b022f55fae25c1cbe29f6b7acb7d4bde38290734ceece348_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "articleauthors/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));

        $__internal_7d46122e1e03c0c9b022f55fae25c1cbe29f6b7acb7d4bde38290734ceece348->leave($__internal_7d46122e1e03c0c9b022f55fae25c1cbe29f6b7acb7d4bde38290734ceece348_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_4236afde49909baeeedc9f81d00265378a316064fde6ac22a4e45230842c71ee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4236afde49909baeeedc9f81d00265378a316064fde6ac22a4e45230842c71ee->enter($__internal_4236afde49909baeeedc9f81d00265378a316064fde6ac22a4e45230842c71ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Articleauthor creation</h1>

    ";
        // line 6
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () {
            throw new Twig_Error_Runtime('Variable "form" does not exist.', 6, $this->getSourceContext());
        })()), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () {
            throw new Twig_Error_Runtime('Variable "form" does not exist.', 7, $this->getSourceContext());
        })()), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () {
            throw new Twig_Error_Runtime('Variable "form" does not exist.', 9, $this->getSourceContext());
        })()), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articleauthors_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";

        $__internal_4236afde49909baeeedc9f81d00265378a316064fde6ac22a4e45230842c71ee->leave($__internal_4236afde49909baeeedc9f81d00265378a316064fde6ac22a4e45230842c71ee_prof);

    }

    public function getTemplateName()
    {
        return "articleauthors/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return [60 => 13, 53 => 9, 48 => 7, 44 => 6, 40 => 4, 34 => 3, 11 => 1,];
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Articleauthor creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('articleauthors_index') }}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}
", "articleauthors/new.html.twig", "/home/serg/restoclub/app/Resources/views/articleauthors/new.html.twig");
    }
}
