<?php

/* articletagged/edit.html.twig */

class __TwigTemplate_7228c51c30add9c0c4c62a2ec55e09bdc13594fcb2722a5a1b3e1f16d9fecafd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "articletagged/edit.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_019e2f67e316292d80cd4dc2b549637829038885a8eaf7d809d6edbb5084e201 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_019e2f67e316292d80cd4dc2b549637829038885a8eaf7d809d6edbb5084e201->enter($__internal_019e2f67e316292d80cd4dc2b549637829038885a8eaf7d809d6edbb5084e201_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "articletagged/edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));

        $__internal_019e2f67e316292d80cd4dc2b549637829038885a8eaf7d809d6edbb5084e201->leave($__internal_019e2f67e316292d80cd4dc2b549637829038885a8eaf7d809d6edbb5084e201_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_fc36393e5e6a7a8458ffde4def49c4209d17a8cbaa5f1dd3a09ee1582d48b42d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fc36393e5e6a7a8458ffde4def49c4209d17a8cbaa5f1dd3a09ee1582d48b42d->enter($__internal_fc36393e5e6a7a8458ffde4def49c4209d17a8cbaa5f1dd3a09ee1582d48b42d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Articletagged edit</h1>

    ";
        // line 6
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["edit_form"]) || array_key_exists("edit_form", $context) ? $context["edit_form"] : (function () {
            throw new Twig_Error_Runtime('Variable "edit_form" does not exist.', 6, $this->getSourceContext());
        })()), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["edit_form"]) || array_key_exists("edit_form", $context) ? $context["edit_form"] : (function () {
            throw new Twig_Error_Runtime('Variable "edit_form" does not exist.', 7, $this->getSourceContext());
        })()), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" />
    ";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["edit_form"]) || array_key_exists("edit_form", $context) ? $context["edit_form"] : (function () {
            throw new Twig_Error_Runtime('Variable "edit_form" does not exist.', 9, $this->getSourceContext());
        })()), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articletagged_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            ";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) || array_key_exists("delete_form", $context) ? $context["delete_form"] : (function () {
            throw new Twig_Error_Runtime('Variable "delete_form" does not exist.', 16, $this->getSourceContext());
        })()), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) || array_key_exists("delete_form", $context) ? $context["delete_form"] : (function () {
            throw new Twig_Error_Runtime('Variable "delete_form" does not exist.', 18, $this->getSourceContext());
        })()), 'form_end');
        echo "
        </li>
    </ul>
";

        $__internal_fc36393e5e6a7a8458ffde4def49c4209d17a8cbaa5f1dd3a09ee1582d48b42d->leave($__internal_fc36393e5e6a7a8458ffde4def49c4209d17a8cbaa5f1dd3a09ee1582d48b42d_prof);

    }

    public function getTemplateName()
    {
        return "articletagged/edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return [71 => 18, 66 => 16, 60 => 13, 53 => 9, 48 => 7, 44 => 6, 40 => 4, 34 => 3, 11 => 1,];
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Articletagged edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" />
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('articletagged_index') }}\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "articletagged/edit.html.twig", "/home/serg/restoclub/app/Resources/views/articletagged/edit.html.twig");
    }
}
