<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_b95924db847db758100dd32700438899d5537f1dc2ce976388ae324193faae65 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = [
            'head'  => [$this, 'block_head'],
            'menu'  => [$this, 'block_menu'],
            'panel' => [$this, 'block_panel'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_7bb13034e3bd40bc16768dcf1656c602a9ca0769b022a91131074a40687fdaf4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7bb13034e3bd40bc16768dcf1656c602a9ca0769b022a91131074a40687fdaf4->enter($__internal_7bb13034e3bd40bc16768dcf1656c602a9ca0769b022a91131074a40687fdaf4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));

        $__internal_7bb13034e3bd40bc16768dcf1656c602a9ca0769b022a91131074a40687fdaf4->leave($__internal_7bb13034e3bd40bc16768dcf1656c602a9ca0769b022a91131074a40687fdaf4_prof);

    }

    // line 3
    public function block_head($context, array $blocks = [])
    {
        $__internal_8d3c0f9572c0e3200a196d2ede135a83ddb3dbeed1086caa483fbd7114b5ef18 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8d3c0f9572c0e3200a196d2ede135a83ddb3dbeed1086caa483fbd7114b5ef18->enter($__internal_8d3c0f9572c0e3200a196d2ede135a83ddb3dbeed1086caa483fbd7114b5ef18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () {
            throw new Twig_Error_Runtime('Variable "collector" does not exist.', 4, $this->getSourceContext());
        })()), "hasexception", [])) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", ["token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () {
                throw new Twig_Error_Runtime('Variable "token" does not exist.', 6, $this->getSourceContext());
            })())]));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";

        $__internal_8d3c0f9572c0e3200a196d2ede135a83ddb3dbeed1086caa483fbd7114b5ef18->leave($__internal_8d3c0f9572c0e3200a196d2ede135a83ddb3dbeed1086caa483fbd7114b5ef18_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = [])
    {
        $__internal_6a70229e978b1e4b3207f3235a5f079adec82aeeb8b7998967d5103002dd11a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6a70229e978b1e4b3207f3235a5f079adec82aeeb8b7998967d5103002dd11a1->enter($__internal_6a70229e978b1e4b3207f3235a5f079adec82aeeb8b7998967d5103002dd11a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () {
            throw new Twig_Error_Runtime('Variable "collector" does not exist.', 13, $this->getSourceContext());
        })()), "hasexception", [])) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () {
            throw new Twig_Error_Runtime('Variable "collector" does not exist.', 16, $this->getSourceContext());
        })()), "hasexception", [])) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";

        $__internal_6a70229e978b1e4b3207f3235a5f079adec82aeeb8b7998967d5103002dd11a1->leave($__internal_6a70229e978b1e4b3207f3235a5f079adec82aeeb8b7998967d5103002dd11a1_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = [])
    {
        $__internal_6902228b2826b3147928d944bc614138431c53b94c171480f74291d76cf4750a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6902228b2826b3147928d944bc614138431c53b94c171480f74291d76cf4750a->enter($__internal_6902228b2826b3147928d944bc614138431c53b94c171480f74291d76cf4750a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if (!twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () {
            throw new Twig_Error_Runtime('Variable "collector" does not exist.', 27, $this->getSourceContext());
        })()), "hasexception", [])) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", ["token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () {
                throw new Twig_Error_Runtime('Variable "token" does not exist.', 33, $this->getSourceContext());
            })())]));
            echo "
        </div>
    ";
        }

        $__internal_6902228b2826b3147928d944bc614138431c53b94c171480f74291d76cf4750a->leave($__internal_6902228b2826b3147928d944bc614138431c53b94c171480f74291d76cf4750a_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return [117 => 33, 114 => 32, 108 => 28, 106 => 27, 102 => 25, 96 => 24, 88 => 21, 82 => 17, 80 => 16, 75 => 14, 70 => 13, 64 => 12, 54 => 9, 48 => 6, 45 => 5, 42 => 4, 36 => 3, 11 => 1,];
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/serg/restoclub/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
