<?php

/* articletagged/show.html.twig */

class __TwigTemplate_ed6d0cfcac4be7df971950e43bb4c225955af234d5b2d14004c901d75b1649bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "articletagged/show.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_d6b2fd4ed2919a740bb22374d563068c20523d39b0f2c675ea45a57fd93988b8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d6b2fd4ed2919a740bb22374d563068c20523d39b0f2c675ea45a57fd93988b8->enter($__internal_d6b2fd4ed2919a740bb22374d563068c20523d39b0f2c675ea45a57fd93988b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "articletagged/show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));

        $__internal_d6b2fd4ed2919a740bb22374d563068c20523d39b0f2c675ea45a57fd93988b8->leave($__internal_d6b2fd4ed2919a740bb22374d563068c20523d39b0f2c675ea45a57fd93988b8_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_1233a2232d6410d793325731380ef32427981a1e85b0e7bf848aa46cd6b05b66 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1233a2232d6410d793325731380ef32427981a1e85b0e7bf848aa46cd6b05b66->enter($__internal_1233a2232d6410d793325731380ef32427981a1e85b0e7bf848aa46cd6b05b66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Articletagged</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleTagged"]) || array_key_exists("articleTagged", $context) ? $context["articleTagged"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleTagged" does not exist.', 10, $this->getSourceContext());
        })()), "id", []), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Title</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleTagged"]) || array_key_exists("articleTagged", $context) ? $context["articleTagged"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleTagged" does not exist.', 14, $this->getSourceContext());
        })()), "title", []), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Url</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleTagged"]) || array_key_exists("articleTagged", $context) ? $context["articleTagged"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleTagged" does not exist.', 18, $this->getSourceContext());
        })()), "url", []), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Text</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleTagged"]) || array_key_exists("articleTagged", $context) ? $context["articleTagged"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleTagged" does not exist.', 22, $this->getSourceContext());
        })()), "text", []), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Created</th>
                <td>";
        // line 26
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleTagged"]) || array_key_exists("articleTagged", $context) ? $context["articleTagged"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleTagged" does not exist.', 26, $this->getSourceContext());
        })()), "created", [])) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleTagged"]) || array_key_exists("articleTagged", $context) ? $context["articleTagged"] : (function () {
                throw new Twig_Error_Runtime('Variable "articleTagged" does not exist.', 26, $this->getSourceContext());
            })()), "created", []), "Y-m-d H:i:s"), "html", null, true);
        }
        echo "</td>
            </tr>
            <tr>
                <th>Published</th>
                <td>";
        // line 30
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleTagged"]) || array_key_exists("articleTagged", $context) ? $context["articleTagged"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleTagged" does not exist.', 30, $this->getSourceContext());
        })()), "published", [])) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleTagged"]) || array_key_exists("articleTagged", $context) ? $context["articleTagged"] : (function () {
                throw new Twig_Error_Runtime('Variable "articleTagged" does not exist.', 30, $this->getSourceContext());
            })()), "published", []), "Y-m-d H:i:s"), "html", null, true);
        }
        echo "</td>
            </tr>
            <tr>
                <th>Updated</th>
                <td>";
        // line 34
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleTagged"]) || array_key_exists("articleTagged", $context) ? $context["articleTagged"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleTagged" does not exist.', 34, $this->getSourceContext());
        })()), "updated", [])) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleTagged"]) || array_key_exists("articleTagged", $context) ? $context["articleTagged"] : (function () {
                throw new Twig_Error_Runtime('Variable "articleTagged" does not exist.', 34, $this->getSourceContext());
            })()), "updated", []), "Y-m-d H:i:s"), "html", null, true);
        }
        echo "</td>
            </tr>
            <tr>
                <th>Tags</th>
                <td>
                    ";
        // line 39
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleTagged"]) || array_key_exists("articleTagged", $context) ? $context["articleTagged"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleTagged" does not exist.', 39, $this->getSourceContext());
        })()), "tags", []));
        foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
            // line 40
            echo "                        <a href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articletagged_tag_show", ["tag" => twig_get_attribute($this->env, $this->getSourceContext(), $context["tag"], "name", [])]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["tag"], "name", []), "html", null, true);
            echo "</a>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "                </td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 49
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articletagged_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articletagged_edit", ["id" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["articleTagged"]) || array_key_exists("articleTagged", $context) ? $context["articleTagged"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleTagged" does not exist.', 52, $this->getSourceContext());
        })()), "id", [])]), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 55
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) || array_key_exists("delete_form", $context) ? $context["delete_form"] : (function () {
            throw new Twig_Error_Runtime('Variable "delete_form" does not exist.', 55, $this->getSourceContext());
        })()), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 57
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) || array_key_exists("delete_form", $context) ? $context["delete_form"] : (function () {
            throw new Twig_Error_Runtime('Variable "delete_form" does not exist.', 57, $this->getSourceContext());
        })()), 'form_end');
        echo "
        </li>
    </ul>
";

        $__internal_1233a2232d6410d793325731380ef32427981a1e85b0e7bf848aa46cd6b05b66->leave($__internal_1233a2232d6410d793325731380ef32427981a1e85b0e7bf848aa46cd6b05b66_prof);

    }

    public function getTemplateName()
    {
        return "articletagged/show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return [145 => 57, 140 => 55, 134 => 52, 128 => 49, 119 => 42, 108 => 40, 104 => 39, 94 => 34, 85 => 30, 76 => 26, 69 => 22, 62 => 18, 55 => 14, 48 => 10, 40 => 4, 34 => 3, 11 => 1,];
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Articletagged</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ articleTagged.id }}</td>
            </tr>
            <tr>
                <th>Title</th>
                <td>{{ articleTagged.title }}</td>
            </tr>
            <tr>
                <th>Url</th>
                <td>{{ articleTagged.url }}</td>
            </tr>
            <tr>
                <th>Text</th>
                <td>{{ articleTagged.text }}</td>
            </tr>
            <tr>
                <th>Created</th>
                <td>{% if articleTagged.created %}{{ articleTagged.created|date('Y-m-d H:i:s') }}{% endif %}</td>
            </tr>
            <tr>
                <th>Published</th>
                <td>{% if articleTagged.published %}{{ articleTagged.published|date('Y-m-d H:i:s') }}{% endif %}</td>
            </tr>
            <tr>
                <th>Updated</th>
                <td>{% if articleTagged.updated %}{{ articleTagged.updated|date('Y-m-d H:i:s') }}{% endif %}</td>
            </tr>
            <tr>
                <th>Tags</th>
                <td>
                    {% for tag in articleTagged.tags %}
                        <a href=\"{{ path('articletagged_tag_show', {'tag' : tag.name}) }}\">{{ tag.name }}</a>
                    {% endfor %}
                </td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('articletagged_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('articletagged_edit', { 'id': articleTagged.id }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", "articletagged/show.html.twig", "/home/serg/restoclub/app/Resources/views/articletagged/show.html.twig");
    }
}
