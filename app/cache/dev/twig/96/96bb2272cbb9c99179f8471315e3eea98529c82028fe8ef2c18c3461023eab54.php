<?php

/* articletagged/new.html.twig */

class __TwigTemplate_7d90c374569be53b5d18d7ea23ea09a76a263ebf675f32bece92f427c20ef35b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "articletagged/new.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_65ddbc5a0f4bad0c268aa7ad264620ef0b2af786f2b2d63035f5dd54073fc5be = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_65ddbc5a0f4bad0c268aa7ad264620ef0b2af786f2b2d63035f5dd54073fc5be->enter($__internal_65ddbc5a0f4bad0c268aa7ad264620ef0b2af786f2b2d63035f5dd54073fc5be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "articletagged/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));

        $__internal_65ddbc5a0f4bad0c268aa7ad264620ef0b2af786f2b2d63035f5dd54073fc5be->leave($__internal_65ddbc5a0f4bad0c268aa7ad264620ef0b2af786f2b2d63035f5dd54073fc5be_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_56b2f2267433cda39be88de3205bd5db158861c9bf4660f95fb68b51521451fc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56b2f2267433cda39be88de3205bd5db158861c9bf4660f95fb68b51521451fc->enter($__internal_56b2f2267433cda39be88de3205bd5db158861c9bf4660f95fb68b51521451fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Articletagged creation</h1>

    ";
        // line 6
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () {
            throw new Twig_Error_Runtime('Variable "form" does not exist.', 6, $this->getSourceContext());
        })()), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () {
            throw new Twig_Error_Runtime('Variable "form" does not exist.', 7, $this->getSourceContext());
        })()), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () {
            throw new Twig_Error_Runtime('Variable "form" does not exist.', 9, $this->getSourceContext());
        })()), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articletagged_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";

        $__internal_56b2f2267433cda39be88de3205bd5db158861c9bf4660f95fb68b51521451fc->leave($__internal_56b2f2267433cda39be88de3205bd5db158861c9bf4660f95fb68b51521451fc_prof);

    }

    public function getTemplateName()
    {
        return "articletagged/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return [60 => 13, 53 => 9, 48 => 7, 44 => 6, 40 => 4, 34 => 3, 11 => 1,];
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Articletagged creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('articletagged_index') }}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}
", "articletagged/new.html.twig", "/home/serg/restoclub/app/Resources/views/articletagged/new.html.twig");
    }
}
