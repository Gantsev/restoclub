<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_a0f267e2bc0b825cecfa66bca0165979a369f62bee351812061352c7a7f40606 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = [
            'head'  => [$this, 'block_head'],
            'title' => [$this, 'block_title'],
            'body'  => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_869c6a24f32af67b5e639d68cc0a4ae46ff0da24ededb42348c18d50e942e83a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_869c6a24f32af67b5e639d68cc0a4ae46ff0da24ededb42348c18d50e942e83a->enter($__internal_869c6a24f32af67b5e639d68cc0a4ae46ff0da24ededb42348c18d50e942e83a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));

        $__internal_869c6a24f32af67b5e639d68cc0a4ae46ff0da24ededb42348c18d50e942e83a->leave($__internal_869c6a24f32af67b5e639d68cc0a4ae46ff0da24ededb42348c18d50e942e83a_prof);

    }

    // line 3
    public function block_head($context, array $blocks = [])
    {
        $__internal_93ed3a700b2d111c83d8071cdf064181d9eb3844e02a7b17794a9778f1e7d493 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93ed3a700b2d111c83d8071cdf064181d9eb3844e02a7b17794a9778f1e7d493->enter($__internal_93ed3a700b2d111c83d8071cdf064181d9eb3844e02a7b17794a9778f1e7d493_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";

        $__internal_93ed3a700b2d111c83d8071cdf064181d9eb3844e02a7b17794a9778f1e7d493->leave($__internal_93ed3a700b2d111c83d8071cdf064181d9eb3844e02a7b17794a9778f1e7d493_prof);

    }

    // line 7
    public function block_title($context, array $blocks = [])
    {
        $__internal_7fb4610ff8bfc0479bd1b629d74ae7447fbb1d20455e6489b57dc92b89b4a0fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7fb4610ff8bfc0479bd1b629d74ae7447fbb1d20455e6489b57dc92b89b4a0fd->enter($__internal_7fb4610ff8bfc0479bd1b629d74ae7447fbb1d20455e6489b57dc92b89b4a0fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () {
            throw new Twig_Error_Runtime('Variable "exception" does not exist.', 8, $this->getSourceContext());
        })()), "message", []), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () {
            throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 8, $this->getSourceContext());
        })()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () {
            throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 8, $this->getSourceContext());
        })()), "html", null, true);
        echo ")
";

        $__internal_7fb4610ff8bfc0479bd1b629d74ae7447fbb1d20455e6489b57dc92b89b4a0fd->leave($__internal_7fb4610ff8bfc0479bd1b629d74ae7447fbb1d20455e6489b57dc92b89b4a0fd_prof);

    }

    // line 11
    public function block_body($context, array $blocks = [])
    {
        $__internal_d8001484f7d2ea2775547b47abd10967bd85e43cd4ff9e0367a250d3c9713402 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8001484f7d2ea2775547b47abd10967bd85e43cd4ff9e0367a250d3c9713402->enter($__internal_d8001484f7d2ea2775547b47abd10967bd85e43cd4ff9e0367a250d3c9713402_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);

        $__internal_d8001484f7d2ea2775547b47abd10967bd85e43cd4ff9e0367a250d3c9713402->leave($__internal_d8001484f7d2ea2775547b47abd10967bd85e43cd4ff9e0367a250d3c9713402_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return [78 => 12, 72 => 11, 58 => 8, 52 => 7, 42 => 4, 36 => 3, 11 => 1,];
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/serg/restoclub/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
