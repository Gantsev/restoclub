<?php

/* articletagged/show_tag.html.twig */

class __TwigTemplate_209f38628b8164993ac77379e2db14b62703d562f09a15c20af3953ab3ce0e30 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "articletagged/show_tag.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_2090303f73deaa007cc2cc1d95b16ffe8e94cd15c382393dd1fff9f1cc5cbb23 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2090303f73deaa007cc2cc1d95b16ffe8e94cd15c382393dd1fff9f1cc5cbb23->enter($__internal_2090303f73deaa007cc2cc1d95b16ffe8e94cd15c382393dd1fff9f1cc5cbb23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "articletagged/show_tag.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));

        $__internal_2090303f73deaa007cc2cc1d95b16ffe8e94cd15c382393dd1fff9f1cc5cbb23->leave($__internal_2090303f73deaa007cc2cc1d95b16ffe8e94cd15c382393dd1fff9f1cc5cbb23_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_619d7e4fbbdad2db41b8775c563f3b97016abeab6b2c897a24ef1e3da3b0a9a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_619d7e4fbbdad2db41b8775c563f3b97016abeab6b2c897a24ef1e3da3b0a9a8->enter($__internal_619d7e4fbbdad2db41b8775c563f3b97016abeab6b2c897a24ef1e3da3b0a9a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Article by tag list</h1>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Url</th>
            <th>Text</th>
            <th>Created</th>
            <th>Published</th>
            <th>Updated</th>
            <th>Tags</th>
        </tr>
        </thead>
        <tbody>
        ";
        // line 19
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["articles"]) || array_key_exists("articles", $context) ? $context["articles"] : (function () {
            throw new Twig_Error_Runtime('Variable "articles" does not exist.', 19, $this->getSourceContext());
        })()));
        foreach ($context['_seq'] as $context["_key"] => $context["article"]) {
            // line 20
            echo "            <tr>
                <td><a href=\"";
            // line 21
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articletagged_show", ["url" => twig_get_attribute($this->env, $this->getSourceContext(), $context["article"], "url", [])]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["article"], "id", []), "html", null, true);
            echo "</a></td>
                <td>";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["article"], "title", []), "html", null, true);
            echo "</td>
                <td>";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["article"], "url", []), "html", null, true);
            echo "</td>
                <td>";
            // line 24
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["article"], "text", []), "html", null, true);
            echo "</td>
                <td>";
            // line 25
            if (twig_get_attribute($this->env, $this->getSourceContext(), $context["article"], "created", [])) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["article"], "created", []), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>";
            // line 26
            if (twig_get_attribute($this->env, $this->getSourceContext(), $context["article"], "published", [])) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["article"], "published", []), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>";
            // line 27
            if (twig_get_attribute($this->env, $this->getSourceContext(), $context["article"], "updated", [])) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["article"], "updated", []), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>
                    ";
            // line 29
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), $context["article"], "tags", []));
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                // line 30
                echo "                        <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articletagged_tag_show", ["tag" => twig_get_attribute($this->env, $this->getSourceContext(), $context["tag"], "name", [])]), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["tag"], "name", []), "html", null, true);
                echo "</a>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 32
            echo "                </td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articletagged_show", ["url" => twig_get_attribute($this->env, $this->getSourceContext(), $context["article"], "url", [])]), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 39
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articletagged_edit", ["id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["article"], "id", [])]), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['article'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "        </tbody>
    </table>
";

        $__internal_619d7e4fbbdad2db41b8775c563f3b97016abeab6b2c897a24ef1e3da3b0a9a8->leave($__internal_619d7e4fbbdad2db41b8775c563f3b97016abeab6b2c897a24ef1e3da3b0a9a8_prof);

    }

    public function getTemplateName()
    {
        return "articletagged/show_tag.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return [140 => 45, 128 => 39, 122 => 36, 116 => 32, 105 => 30, 101 => 29, 94 => 27, 88 => 26, 82 => 25, 78 => 24, 74 => 23, 70 => 22, 64 => 21, 61 => 20, 57 => 19, 40 => 4, 34 => 3, 11 => 1,];
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Article by tag list</h1>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Url</th>
            <th>Text</th>
            <th>Created</th>
            <th>Published</th>
            <th>Updated</th>
            <th>Tags</th>
        </tr>
        </thead>
        <tbody>
        {% for article in articles %}
            <tr>
                <td><a href=\"{{ path('articletagged_show', { 'url': article.url }) }}\">{{ article.id }}</a></td>
                <td>{{ article.title }}</td>
                <td>{{ article.url }}</td>
                <td>{{ article.text }}</td>
                <td>{% if article.created %}{{ article.created|date('Y-m-d H:i:s') }}{% endif %}</td>
                <td>{% if article.published %}{{ article.published|date('Y-m-d H:i:s') }}{% endif %}</td>
                <td>{% if article.updated %}{{ article.updated|date('Y-m-d H:i:s') }}{% endif %}</td>
                <td>
                    {% for tag in article.tags %}
                        <a href=\"{{ path('articletagged_tag_show', {'tag' : tag.name}) }}\">{{ tag.name }}</a>
                    {% endfor %}
                </td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('articletagged_show', { 'url': article.url }) }}\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('articletagged_edit', { 'id': article.id }) }}\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
{% endblock %}
", "articletagged/show_tag.html.twig", "/home/serg/restoclub/app/Resources/views/articletagged/show_tag.html.twig");
    }
}
