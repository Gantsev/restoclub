<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_bb16925de7d010282d9b8e1c044cc5d2353b0261a56b600c32b7881baec96f29 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = [
            'toolbar' => [$this, 'block_toolbar'],
            'menu'    => [$this, 'block_menu'],
            'panel'   => [$this, 'block_panel'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_b865c62dcc4a65fdad33f8b9d2019f4bd58c7e1890571f0b8020912405c3ec0c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b865c62dcc4a65fdad33f8b9d2019f4bd58c7e1890571f0b8020912405c3ec0c->enter($__internal_b865c62dcc4a65fdad33f8b9d2019f4bd58c7e1890571f0b8020912405c3ec0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));

        $__internal_b865c62dcc4a65fdad33f8b9d2019f4bd58c7e1890571f0b8020912405c3ec0c->leave($__internal_b865c62dcc4a65fdad33f8b9d2019f4bd58c7e1890571f0b8020912405c3ec0c_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = [])
    {
        $__internal_78bf256013469278542b9fba306c754b247e9a4fb6a38eeaf59c64b12fc57bdd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78bf256013469278542b9fba306c754b247e9a4fb6a38eeaf59c64b12fc57bdd->enter($__internal_78bf256013469278542b9fba306c754b247e9a4fb6a38eeaf59c64b12fc57bdd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));


        $__internal_78bf256013469278542b9fba306c754b247e9a4fb6a38eeaf59c64b12fc57bdd->leave($__internal_78bf256013469278542b9fba306c754b247e9a4fb6a38eeaf59c64b12fc57bdd_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = [])
    {
        $__internal_e93e2f61cb29051d8fd9f0a8922ff287ffdf859e131695f965745da579bd31a9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e93e2f61cb29051d8fd9f0a8922ff287ffdf859e131695f965745da579bd31a9->enter($__internal_e93e2f61cb29051d8fd9f0a8922ff287ffdf859e131695f965745da579bd31a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";

        $__internal_e93e2f61cb29051d8fd9f0a8922ff287ffdf859e131695f965745da579bd31a9->leave($__internal_e93e2f61cb29051d8fd9f0a8922ff287ffdf859e131695f965745da579bd31a9_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = [])
    {
        $__internal_7a0d26c42d1920d415de2832233d63416d82811f2c31659546fc0203b628cf88 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a0d26c42d1920d415de2832233d63416d82811f2c31659546fc0203b628cf88->enter($__internal_7a0d26c42d1920d415de2832233d63416d82811f2c31659546fc0203b628cf88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", ["token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () {
            throw new Twig_Error_Runtime('Variable "token" does not exist.', 13, $this->getSourceContext());
        })())]));
        echo "
";

        $__internal_7a0d26c42d1920d415de2832233d63416d82811f2c31659546fc0203b628cf88->leave($__internal_7a0d26c42d1920d415de2832233d63416d82811f2c31659546fc0203b628cf88_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return [73 => 13, 67 => 12, 56 => 7, 53 => 6, 47 => 5, 36 => 3, 11 => 1,];
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/serg/restoclub/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
