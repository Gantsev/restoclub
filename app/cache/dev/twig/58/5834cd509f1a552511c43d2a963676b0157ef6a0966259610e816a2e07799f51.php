<?php

/* @Twig/Exception/traces.txt.twig */
class __TwigTemplate_208ef501e84db04a266942aa13471e8a5ab596f0fcb65cbde872f55045de75b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_e544ec0e2fdd76c720fe9d032f60360edf8ea004d5256eb77f5f67a67703c9e6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e544ec0e2fdd76c720fe9d032f60360edf8ea004d5256eb77f5f67a67703c9e6->enter($__internal_e544ec0e2fdd76c720fe9d032f60360edf8ea004d5256eb77f5f67a67703c9e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        // line 1
        if (twig_length_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () {
            throw new Twig_Error_Runtime('Variable "exception" does not exist.', 1, $this->getSourceContext());
        })()), "trace", []))) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () {
                throw new Twig_Error_Runtime('Variable "exception" does not exist.', 2, $this->getSourceContext());
            })()), "trace", []));
            foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
                // line 3
                $this->loadTemplate("@Twig/Exception/trace.txt.twig", "@Twig/Exception/traces.txt.twig", 3)->display(["trace" => $context["trace"]]);
                // line 4
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }

        $__internal_e544ec0e2fdd76c720fe9d032f60360edf8ea004d5256eb77f5f67a67703c9e6->leave($__internal_e544ec0e2fdd76c720fe9d032f60360edf8ea004d5256eb77f5f67a67703c9e6_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/traces.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return [30 => 4, 28 => 3, 24 => 2, 22 => 1,];
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if exception.trace|length %}
{% for trace in exception.trace %}
{% include '@Twig/Exception/trace.txt.twig' with { 'trace': trace } only %}

{% endfor %}
{% endif %}
", "@Twig/Exception/traces.txt.twig", "/home/serg/restoclub/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/traces.txt.twig");
    }
}
