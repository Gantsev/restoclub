<?php

/* @WebProfiler/Collector/twig.html.twig */
class __TwigTemplate_59bbf15bb96b1895144ebf18c764b1f5c0c5070b2cb91c47d49d218c4f02b30b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/twig.html.twig", 1);
        $this->blocks = [
            'toolbar' => [$this, 'block_toolbar'],
            'menu'    => [$this, 'block_menu'],
            'panel'   => [$this, 'block_panel'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_19f1ddaadcf1c2c2259e4ba470449042a11668c29b26fe88600908e759f0fe7b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_19f1ddaadcf1c2c2259e4ba470449042a11668c29b26fe88600908e759f0fe7b->enter($__internal_19f1ddaadcf1c2c2259e4ba470449042a11668c29b26fe88600908e759f0fe7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/twig.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));

        $__internal_19f1ddaadcf1c2c2259e4ba470449042a11668c29b26fe88600908e759f0fe7b->leave($__internal_19f1ddaadcf1c2c2259e4ba470449042a11668c29b26fe88600908e759f0fe7b_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = [])
    {
        $__internal_17a79b1e783dc06a44654271ff5b81af3223eb68284316f932403c18baaff03d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_17a79b1e783dc06a44654271ff5b81af3223eb68284316f932403c18baaff03d->enter($__internal_17a79b1e783dc06a44654271ff5b81af3223eb68284316f932403c18baaff03d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        $context["time"] = ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () {
            throw new Twig_Error_Runtime('Variable "collector" does not exist.', 4, $this->getSourceContext());
        })()), "templatecount", [])) ? (sprintf("%0.0f", twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () {
            throw new Twig_Error_Runtime('Variable "collector" does not exist.', 4, $this->getSourceContext());
        })()), "time", []))) : ("n/a"));
        // line 5
        echo "    ";
        ob_start();
        // line 6
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/twig.svg");
        echo "
        <span class=\"sf-toolbar-value\">";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["time"]) || array_key_exists("time", $context) ? $context["time"] : (function () {
            throw new Twig_Error_Runtime('Variable "time" does not exist.', 7, $this->getSourceContext());
        })()), "html", null, true);
        echo "</span>
        <span class=\"sf-toolbar-label\">ms</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 10
        echo "
    ";
        // line 11
        ob_start();
        // line 12
        echo "        <div class=\"sf-toolbar-info-piece\">
            <b>Render Time</b>
            <span>";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["time"]) || array_key_exists("time", $context) ? $context["time"] : (function () {
            throw new Twig_Error_Runtime('Variable "time" does not exist.', 14, $this->getSourceContext());
        })()), "html", null, true);
        echo " ms</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Template Calls</b>
            <span class=\"sf-toolbar-status\">";
        // line 18
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () {
            throw new Twig_Error_Runtime('Variable "collector" does not exist.', 18, $this->getSourceContext());
        })()), "templatecount", []), "html", null, true);
        echo "</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Block Calls</b>
            <span class=\"sf-toolbar-status\">";
        // line 22
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () {
            throw new Twig_Error_Runtime('Variable "collector" does not exist.', 22, $this->getSourceContext());
        })()), "blockcount", []), "html", null, true);
        echo "</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Macro Calls</b>
            <span class=\"sf-toolbar-status\">";
        // line 26
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () {
            throw new Twig_Error_Runtime('Variable "collector" does not exist.', 26, $this->getSourceContext());
        })()), "macrocount", []), "html", null, true);
        echo "</span>
        </div>
    ";
        $context["text"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", ["link" => (isset($context["profiler_url"]) || array_key_exists("profiler_url", $context) ? $context["profiler_url"] : (function () {
            throw new Twig_Error_Runtime('Variable "profiler_url" does not exist.', 30, $this->getSourceContext());
        })())]);
        echo "
";

        $__internal_17a79b1e783dc06a44654271ff5b81af3223eb68284316f932403c18baaff03d->leave($__internal_17a79b1e783dc06a44654271ff5b81af3223eb68284316f932403c18baaff03d_prof);

    }

    // line 33
    public function block_menu($context, array $blocks = [])
    {
        $__internal_bbb5e817e04a6e5f90ac694f07f263ec26e1ac63ec243190cea1fb0eae0a50ea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bbb5e817e04a6e5f90ac694f07f263ec26e1ac63ec243190cea1fb0eae0a50ea->enter($__internal_bbb5e817e04a6e5f90ac694f07f263ec26e1ac63ec243190cea1fb0eae0a50ea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 34
        echo "    <span class=\"label\">
        <span class=\"icon\">";
        // line 35
        echo twig_include($this->env, $context, "@WebProfiler/Icon/twig.svg");
        echo "</span>
        <strong>Twig</strong>
    </span>
";

        $__internal_bbb5e817e04a6e5f90ac694f07f263ec26e1ac63ec243190cea1fb0eae0a50ea->leave($__internal_bbb5e817e04a6e5f90ac694f07f263ec26e1ac63ec243190cea1fb0eae0a50ea_prof);

    }

    // line 40
    public function block_panel($context, array $blocks = [])
    {
        $__internal_061cb3e3f57671f9d4131b2e345cad682175c1e6201f65920d0e2b33ea53ff1c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_061cb3e3f57671f9d4131b2e345cad682175c1e6201f65920d0e2b33ea53ff1c->enter($__internal_061cb3e3f57671f9d4131b2e345cad682175c1e6201f65920d0e2b33ea53ff1c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 41
        echo "    ";
        if ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () {
                throw new Twig_Error_Runtime('Variable "collector" does not exist.', 41, $this->getSourceContext());
            })()), "templatecount", []) == 0)) {
            // line 42
            echo "        <h2>Twig</h2>

        <div class=\"empty\">
            <p>No Twig templates were rendered for this request.</p>
        </div>
    ";
        } else {
            // line 48
            echo "        <h2>Twig Metrics</h2>

        <div class=\"metrics\">
            <div class=\"metric\">
                <span class=\"value\">";
            // line 52
            echo twig_escape_filter($this->env, sprintf("%0.0f", twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () {
                throw new Twig_Error_Runtime('Variable "collector" does not exist.', 52, $this->getSourceContext());
            })()), "time", [])), "html", null, true);
            echo " <span class=\"unit\">ms</span></span>
                <span class=\"label\">Render time</span>
            </div>

            <div class=\"metric\">
                <span class=\"value\">";
            // line 57
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () {
                throw new Twig_Error_Runtime('Variable "collector" does not exist.', 57, $this->getSourceContext());
            })()), "templatecount", []), "html", null, true);
            echo "</span>
                <span class=\"label\">Template calls</span>
            </div>

            <div class=\"metric\">
                <span class=\"value\">";
            // line 62
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () {
                throw new Twig_Error_Runtime('Variable "collector" does not exist.', 62, $this->getSourceContext());
            })()), "blockcount", []), "html", null, true);
            echo "</span>
                <span class=\"label\">Block calls</span>
            </div>

            <div class=\"metric\">
                <span class=\"value\">";
            // line 67
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () {
                throw new Twig_Error_Runtime('Variable "collector" does not exist.', 67, $this->getSourceContext());
            })()), "macrocount", []), "html", null, true);
            echo "</span>
                <span class=\"label\">Macro calls</span>
            </div>
        </div>

        <p class=\"help\">
            Render time includes sub-requests rendering time (if any).
        </p>

        <h2>Rendered Templates</h2>

        <table>
            <thead>
                <tr>
                    <th scope=\"col\">Template Name</th>
                    <th scope=\"col\">Render Count</th>
                </tr>
            </thead>
            <tbody>
            ";
            // line 86
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () {
                throw new Twig_Error_Runtime('Variable "collector" does not exist.', 86, $this->getSourceContext());
            })()), "templates", []));
            foreach ($context['_seq'] as $context["template"] => $context["count"]) {
                // line 87
                echo "                <tr>
                    <td>";
                // line 88
                echo twig_escape_filter($this->env, $context["template"], "html", null, true);
                echo "</td>
                    <td class=\"font-normal\">";
                // line 89
                echo twig_escape_filter($this->env, $context["count"], "html", null, true);
                echo "</td>
                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['template'], $context['count'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 92
            echo "            </tbody>
        </table>

        <h2>Rendering Call Graph</h2>

        <div id=\"twig-dump\">
            ";
            // line 98
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () {
                throw new Twig_Error_Runtime('Variable "collector" does not exist.', 98, $this->getSourceContext());
            })()), "htmlcallgraph", []), "html", null, true);
            echo "
        </div>
    ";
        }

        $__internal_061cb3e3f57671f9d4131b2e345cad682175c1e6201f65920d0e2b33ea53ff1c->leave($__internal_061cb3e3f57671f9d4131b2e345cad682175c1e6201f65920d0e2b33ea53ff1c_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/twig.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return [224 => 98, 216 => 92, 207 => 89, 203 => 88, 200 => 87, 196 => 86, 174 => 67, 166 => 62, 158 => 57, 150 => 52, 144 => 48, 136 => 42, 133 => 41, 127 => 40, 116 => 35, 113 => 34, 107 => 33, 98 => 30, 95 => 29, 89 => 26, 82 => 22, 75 => 18, 68 => 14, 64 => 12, 62 => 11, 59 => 10, 53 => 7, 48 => 6, 45 => 5, 42 => 4, 36 => 3, 11 => 1,];
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set time = collector.templatecount ? '%0.0f'|format(collector.time) : 'n/a' %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/twig.svg') }}
        <span class=\"sf-toolbar-value\">{{ time }}</span>
        <span class=\"sf-toolbar-label\">ms</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b>Render Time</b>
            <span>{{ time }} ms</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Template Calls</b>
            <span class=\"sf-toolbar-status\">{{ collector.templatecount }}</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Block Calls</b>
            <span class=\"sf-toolbar-status\">{{ collector.blockcount }}</span>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <b>Macro Calls</b>
            <span class=\"sf-toolbar-status\">{{ collector.macrocount }}</span>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: profiler_url }) }}
{% endblock %}

{% block menu %}
    <span class=\"label\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/twig.svg') }}</span>
        <strong>Twig</strong>
    </span>
{% endblock %}

{% block panel %}
    {% if collector.templatecount == 0 %}
        <h2>Twig</h2>

        <div class=\"empty\">
            <p>No Twig templates were rendered for this request.</p>
        </div>
    {% else %}
        <h2>Twig Metrics</h2>

        <div class=\"metrics\">
            <div class=\"metric\">
                <span class=\"value\">{{ '%0.0f'|format(collector.time) }} <span class=\"unit\">ms</span></span>
                <span class=\"label\">Render time</span>
            </div>

            <div class=\"metric\">
                <span class=\"value\">{{ collector.templatecount }}</span>
                <span class=\"label\">Template calls</span>
            </div>

            <div class=\"metric\">
                <span class=\"value\">{{ collector.blockcount }}</span>
                <span class=\"label\">Block calls</span>
            </div>

            <div class=\"metric\">
                <span class=\"value\">{{ collector.macrocount }}</span>
                <span class=\"label\">Macro calls</span>
            </div>
        </div>

        <p class=\"help\">
            Render time includes sub-requests rendering time (if any).
        </p>

        <h2>Rendered Templates</h2>

        <table>
            <thead>
                <tr>
                    <th scope=\"col\">Template Name</th>
                    <th scope=\"col\">Render Count</th>
                </tr>
            </thead>
            <tbody>
            {% for template, count in collector.templates %}
                <tr>
                    <td>{{ template }}</td>
                    <td class=\"font-normal\">{{ count }}</td>
                </tr>
            {% endfor %}
            </tbody>
        </table>

        <h2>Rendering Call Graph</h2>

        <div id=\"twig-dump\">
            {{ collector.htmlcallgraph }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/twig.html.twig", "/home/serg/restoclub/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/twig.html.twig");
    }
}
