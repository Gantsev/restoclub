<?php

/* @Twig/Exception/trace.txt.twig */
class __TwigTemplate_fb498a05edb0519bfb49d71779ffa4305f1b26e71b809741f48ef70f3a5c9ef1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_bc4dcd919928ac053b614e76ede452bd2d2107f85f529a907881097cac55b918 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bc4dcd919928ac053b614e76ede452bd2d2107f85f529a907881097cac55b918->enter($__internal_bc4dcd919928ac053b614e76ede452bd2d2107f85f529a907881097cac55b918_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/trace.txt.twig"));

        // line 1
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || array_key_exists("trace", $context) ? $context["trace"] : (function () {
            throw new Twig_Error_Runtime('Variable "trace" does not exist.', 1, $this->getSourceContext());
        })()), "function", [])) {
            // line 2
            echo "    at ";
            echo((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || array_key_exists("trace", $context) ? $context["trace"] : (function () {
                        throw new Twig_Error_Runtime('Variable "trace" does not exist.', 2, $this->getSourceContext());
                    })()), "class", []) . twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || array_key_exists("trace", $context) ? $context["trace"] : (function () {
                        throw new Twig_Error_Runtime('Variable "trace" does not exist.', 2, $this->getSourceContext());
                    })()), "type", [])) . twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || array_key_exists("trace", $context) ? $context["trace"] : (function () {
                    throw new Twig_Error_Runtime('Variable "trace" does not exist.', 2, $this->getSourceContext());
                })()), "function", []));
            echo "(";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->formatArgsAsText(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || array_key_exists("trace", $context) ? $context["trace"] : (function () {
                throw new Twig_Error_Runtime('Variable "trace" does not exist.', 2, $this->getSourceContext());
            })()), "args", []));
            echo ")
";
        } else {
            // line 4
            echo "    at n/a
";
        }
        // line 6
        if ((twig_get_attribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "file", [], "any", true, true) && twig_get_attribute($this->env, $this->getSourceContext(), ($context["trace"] ?? null), "line", [], "any", true, true))) {
            // line 7
            echo "        in ";
            echo twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || array_key_exists("trace", $context) ? $context["trace"] : (function () {
                throw new Twig_Error_Runtime('Variable "trace" does not exist.', 7, $this->getSourceContext());
            })()), "file", []);
            echo " line ";
            echo twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["trace"]) || array_key_exists("trace", $context) ? $context["trace"] : (function () {
                throw new Twig_Error_Runtime('Variable "trace" does not exist.', 7, $this->getSourceContext());
            })()), "line", []);
            echo "
";
        }

        $__internal_bc4dcd919928ac053b614e76ede452bd2d2107f85f529a907881097cac55b918->leave($__internal_bc4dcd919928ac053b614e76ede452bd2d2107f85f529a907881097cac55b918_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/trace.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return [38 => 7, 36 => 6, 32 => 4, 24 => 2, 22 => 1,];
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if trace.function %}
    at {{ trace.class ~ trace.type ~ trace.function }}({{ trace.args|format_args_as_text }})
{% else %}
    at n/a
{% endif %}
{% if trace.file is defined and trace.line is defined %}
        in {{ trace.file }} line {{ trace.line }}
{% endif %}
", "@Twig/Exception/trace.txt.twig", "/home/serg/restoclub/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/trace.txt.twig");
    }
}
