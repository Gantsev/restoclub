<?php

/* articleauthors/index.html.twig */

class __TwigTemplate_1fe1c5f0837d9ce0ff898d04a69d0aa04834b51771d89f04ffa4d4fb3747d779 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "articleauthors/index.html.twig", 1);
        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_17eaed681007b212ea40e13d19b3ee0dd1d354cf5ce8acdccff0c1051640794c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_17eaed681007b212ea40e13d19b3ee0dd1d354cf5ce8acdccff0c1051640794c->enter($__internal_17eaed681007b212ea40e13d19b3ee0dd1d354cf5ce8acdccff0c1051640794c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "articleauthors/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));

        $__internal_17eaed681007b212ea40e13d19b3ee0dd1d354cf5ce8acdccff0c1051640794c->leave($__internal_17eaed681007b212ea40e13d19b3ee0dd1d354cf5ce8acdccff0c1051640794c_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $__internal_f130ee9af1f5ef3e74123b3604282030e3046dc20aa3a8b852f392c7f2f7d1e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f130ee9af1f5ef3e74123b3604282030e3046dc20aa3a8b852f392c7f2f7d1e7->enter($__internal_f130ee9af1f5ef3e74123b3604282030e3046dc20aa3a8b852f392c7f2f7d1e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Articleauthors list</h1>

    <table>
        <thead>
            <tr>
                <th>Author</th>
                <th>Site</th>
                <th>Id</th>
                <th>Title</th>
                <th>Url</th>
                <th>Text</th>
                <th>Created</th>
                <th>Published</th>
                <th>Updated</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 22
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["articleAuthors"]) || array_key_exists("articleAuthors", $context) ? $context["articleAuthors"] : (function () {
            throw new Twig_Error_Runtime('Variable "articleAuthors" does not exist.', 22, $this->getSourceContext());
        })()));
        foreach ($context['_seq'] as $context["_key"] => $context["articleAuthor"]) {
            // line 23
            echo "            <tr>
                <td><a href=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articleauthors_show", ["url" => twig_get_attribute($this->env, $this->getSourceContext(), $context["articleAuthor"], "url", [])]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["articleAuthor"], "author", []), "html", null, true);
            echo "</a></td>
                <td>";
            // line 25
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["articleAuthor"], "site", []), "html", null, true);
            echo "</td>
                <td>";
            // line 26
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["articleAuthor"], "id", []), "html", null, true);
            echo "</td>
                <td>";
            // line 27
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["articleAuthor"], "title", []), "html", null, true);
            echo "</td>
                <td>";
            // line 28
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["articleAuthor"], "url", []), "html", null, true);
            echo "</td>
                <td>";
            // line 29
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["articleAuthor"], "text", []), "html", null, true);
            echo "</td>
                <td>";
            // line 30
            if (twig_get_attribute($this->env, $this->getSourceContext(), $context["articleAuthor"], "created", [])) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["articleAuthor"], "created", []), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>";
            // line 31
            if (twig_get_attribute($this->env, $this->getSourceContext(), $context["articleAuthor"], "published", [])) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["articleAuthor"], "published", []), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>";
            // line 32
            if (twig_get_attribute($this->env, $this->getSourceContext(), $context["articleAuthor"], "updated", [])) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["articleAuthor"], "updated", []), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articleauthors_show", ["url" => twig_get_attribute($this->env, $this->getSourceContext(), $context["articleAuthor"], "url", [])]), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 39
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articleauthors_edit", ["id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["articleAuthor"], "id", [])]), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['articleAuthor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 50
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("articleauthors_new");
        echo "\">Create a new articleAuthor</a>
        </li>
    </ul>
";

        $__internal_f130ee9af1f5ef3e74123b3604282030e3046dc20aa3a8b852f392c7f2f7d1e7->leave($__internal_f130ee9af1f5ef3e74123b3604282030e3046dc20aa3a8b852f392c7f2f7d1e7_prof);

    }

    public function getTemplateName()
    {
        return "articleauthors/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return [139 => 50, 132 => 45, 120 => 39, 114 => 36, 105 => 32, 99 => 31, 93 => 30, 89 => 29, 85 => 28, 81 => 27, 77 => 26, 73 => 25, 67 => 24, 64 => 23, 60 => 22, 40 => 4, 34 => 3, 11 => 1,];
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Articleauthors list</h1>

    <table>
        <thead>
            <tr>
                <th>Author</th>
                <th>Site</th>
                <th>Id</th>
                <th>Title</th>
                <th>Url</th>
                <th>Text</th>
                <th>Created</th>
                <th>Published</th>
                <th>Updated</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for articleAuthor in articleAuthors %}
            <tr>
                <td><a href=\"{{ path('articleauthors_show', { 'url': articleAuthor.url }) }}\">{{ articleAuthor.author }}</a></td>
                <td>{{ articleAuthor.site }}</td>
                <td>{{ articleAuthor.id }}</td>
                <td>{{ articleAuthor.title }}</td>
                <td>{{ articleAuthor.url }}</td>
                <td>{{ articleAuthor.text }}</td>
                <td>{% if articleAuthor.created %}{{ articleAuthor.created|date('Y-m-d H:i:s') }}{% endif %}</td>
                <td>{% if articleAuthor.published %}{{ articleAuthor.published|date('Y-m-d H:i:s') }}{% endif %}</td>
                <td>{% if articleAuthor.updated %}{{ articleAuthor.updated|date('Y-m-d H:i:s') }}{% endif %}</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('articleauthors_show', { 'url': articleAuthor.url }) }}\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('articleauthors_edit', { 'id': articleAuthor.id }) }}\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('articleauthors_new') }}\">Create a new articleAuthor</a>
        </li>
    </ul>
{% endblock %}
", "articleauthors/index.html.twig", "/home/serg/restoclub/app/Resources/views/articleauthors/index.html.twig");
    }
}
