<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevDebugProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = [];
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => '_wdt']), ['_controller' => 'web_profiler.controller.profiler:toolbarAction',]);
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo . '/', '_profiler_home');
                    }

                    return ['_controller' => 'web_profiler.controller.profiler:homeAction', '_route' => '_profiler_home',];
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return ['_controller' => 'web_profiler.controller.profiler:searchAction', '_route' => '_profiler_search',];
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return ['_controller' => 'web_profiler.controller.profiler:searchBarAction', '_route' => '_profiler_search_bar',];
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return ['_controller' => 'web_profiler.controller.profiler:purgeAction', '_route' => '_profiler_purge',];
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_info']), ['_controller' => 'web_profiler.controller.profiler:infoAction',]);
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return ['_controller' => 'web_profiler.controller.profiler:phpinfoAction', '_route' => '_profiler_phpinfo',];
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_search_results']), ['_controller' => 'web_profiler.controller.profiler:searchResultsAction',]);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler']), ['_controller' => 'web_profiler.controller.profiler:panelAction',]);
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_router']), ['_controller' => 'web_profiler.controller.router:panelAction',]);
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_exception']), ['_controller' => 'web_profiler.controller.exception:showAction',]);
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, ['_route' => '_profiler_exception_css']), ['_controller' => 'web_profiler.controller.exception:cssAction',]);
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, ['_route' => '_twig_error_test']), ['_controller' => 'twig.controller.preview_error:previewErrorPageAction', '_format' => 'html',]);
            }

        }

        if (0 === strpos($pathinfo, '/article')) {
            if (0 === strpos($pathinfo, '/articletagged')) {
                // articletagged_index
                if (rtrim($pathinfo, '/') === '/articletagged') {
                    if (!in_array($this->context->getMethod(), ['GET', 'HEAD'])) {
                        $allow = array_merge($allow, ['GET', 'HEAD']);
                        goto not_articletagged_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo . '/', 'articletagged_index');
                    }

                    return ['_controller' => 'ArticleBundle\\Controller\\ArticleTaggedController::indexAction', '_route' => 'articletagged_index',];
                }
                not_articletagged_index:

                // articletagged_new
                if ($pathinfo === '/articletagged/new') {
                    if (!in_array($this->context->getMethod(), ['GET', 'POST', 'HEAD'])) {
                        $allow = array_merge($allow, ['GET', 'POST', 'HEAD']);
                        goto not_articletagged_new;
                    }

                    return ['_controller' => 'ArticleBundle\\Controller\\ArticleTaggedController::newAction', '_route' => 'articletagged_new',];
                }
                not_articletagged_new:

                // articletagged_show
                if (preg_match('#^/articletagged/(?P<url>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), ['GET', 'HEAD'])) {
                        $allow = array_merge($allow, ['GET', 'HEAD']);
                        goto not_articletagged_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'articletagged_show']), ['_controller' => 'ArticleBundle\\Controller\\ArticleTaggedController::showAction',]);
                }
                not_articletagged_show:

                // articletagged_tag_show
                if (0 === strpos($pathinfo, '/articletagged/tag') && preg_match('#^/articletagged/tag/(?P<tag>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), ['GET', 'HEAD'])) {
                        $allow = array_merge($allow, ['GET', 'HEAD']);
                        goto not_articletagged_tag_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'articletagged_tag_show']), ['_controller' => 'ArticleBundle\\Controller\\ArticleTaggedController::tagAction',]);
                }
                not_articletagged_tag_show:

                // articletagged_edit
                if (preg_match('#^/articletagged/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), ['GET', 'POST', 'HEAD'])) {
                        $allow = array_merge($allow, ['GET', 'POST', 'HEAD']);
                        goto not_articletagged_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'articletagged_edit']), ['_controller' => 'ArticleBundle\\Controller\\ArticleTaggedController::editAction',]);
                }
                not_articletagged_edit:

                // articletagged_delete
                if (preg_match('#^/articletagged/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_articletagged_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'articletagged_delete']), ['_controller' => 'ArticleBundle\\Controller\\ArticleTaggedController::deleteAction',]);
                }
                not_articletagged_delete:

            }

            if (0 === strpos($pathinfo, '/articleauthors')) {
                // articleauthors_index
                if (rtrim($pathinfo, '/') === '/articleauthors') {
                    if (!in_array($this->context->getMethod(), ['GET', 'HEAD'])) {
                        $allow = array_merge($allow, ['GET', 'HEAD']);
                        goto not_articleauthors_index;
                    }

                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo . '/', 'articleauthors_index');
                    }

                    return ['_controller' => 'ArticleBundle\\Controller\\ArticleAuthorsController::indexAction', '_route' => 'articleauthors_index',];
                }
                not_articleauthors_index:

                // articleauthors_new
                if ($pathinfo === '/articleauthors/new') {
                    if (!in_array($this->context->getMethod(), ['GET', 'POST', 'HEAD'])) {
                        $allow = array_merge($allow, ['GET', 'POST', 'HEAD']);
                        goto not_articleauthors_new;
                    }

                    return ['_controller' => 'ArticleBundle\\Controller\\ArticleAuthorsController::newAction', '_route' => 'articleauthors_new',];
                }
                not_articleauthors_new:

                // articleauthors_show
                if (preg_match('#^/articleauthors/(?P<url>[^/]++)$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), ['GET', 'HEAD'])) {
                        $allow = array_merge($allow, ['GET', 'HEAD']);
                        goto not_articleauthors_show;
                    }

                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'articleauthors_show']), ['_controller' => 'ArticleBundle\\Controller\\ArticleAuthorsController::showAction',]);
                }
                not_articleauthors_show:

                // articleauthors_edit
                if (preg_match('#^/articleauthors/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                    if (!in_array($this->context->getMethod(), ['GET', 'POST', 'HEAD'])) {
                        $allow = array_merge($allow, ['GET', 'POST', 'HEAD']);
                        goto not_articleauthors_edit;
                    }

                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'articleauthors_edit']), ['_controller' => 'ArticleBundle\\Controller\\ArticleAuthorsController::editAction',]);
                }
                not_articleauthors_edit:

                // articleauthors_delete
                if (preg_match('#^/articleauthors/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                    if ($this->context->getMethod() != 'DELETE') {
                        $allow[] = 'DELETE';
                        goto not_articleauthors_delete;
                    }

                    return $this->mergeDefaults(array_replace($matches, ['_route' => 'articleauthors_delete']), ['_controller' => 'ArticleBundle\\Controller\\ArticleAuthorsController::deleteAction',]);
                }
                not_articleauthors_delete:

            }

        }

        // article_index
        if (rtrim($pathinfo, '/') === '') {
            if (!in_array($this->context->getMethod(), ['GET', 'HEAD'])) {
                $allow = array_merge($allow, ['GET', 'HEAD']);
                goto not_article_index;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo . '/', 'article_index');
            }

            return ['_controller' => 'ArticleBundle\\Controller\\ArticleController::indexAction', '_route' => 'article_index',];
        }
        not_article_index:

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
