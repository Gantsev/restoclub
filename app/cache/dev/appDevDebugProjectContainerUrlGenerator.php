<?php

use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Psr\Log\LoggerInterface;

/**
 * appDevDebugProjectContainerUrlGenerator
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlGenerator extends Symfony\Component\Routing\Generator\UrlGenerator
{
    private static $declaredRoutes;

    /**
     * Constructor.
     */
    public function __construct(RequestContext $context, LoggerInterface $logger = null)
    {
        $this->context = $context;
        $this->logger = $logger;
        if (null === self::$declaredRoutes) {
            self::$declaredRoutes = [
                '_wdt'                     => [0 => [0 => 'token',], 1 => ['_controller' => 'web_profiler.controller.profiler:toolbarAction',], 2 => [], 3 => [0 => [0 => 'variable', 1 => '/', 2 => '[^/]++', 3 => 'token',], 1 => [0 => 'text', 1 => '/_wdt',],], 4 => [], 5 => [],],
                '_profiler_home'           => [0 => [], 1 => ['_controller' => 'web_profiler.controller.profiler:homeAction',], 2 => [], 3 => [0 => [0 => 'text', 1 => '/_profiler/',],], 4 => [], 5 => [],],
                '_profiler_search'         => [0 => [], 1 => ['_controller' => 'web_profiler.controller.profiler:searchAction',], 2 => [], 3 => [0 => [0 => 'text', 1 => '/_profiler/search',],], 4 => [], 5 => [],],
                '_profiler_search_bar'     => [0 => [], 1 => ['_controller' => 'web_profiler.controller.profiler:searchBarAction',], 2 => [], 3 => [0 => [0 => 'text', 1 => '/_profiler/search_bar',],], 4 => [], 5 => [],],
                '_profiler_purge'          => [0 => [], 1 => ['_controller' => 'web_profiler.controller.profiler:purgeAction',], 2 => [], 3 => [0 => [0 => 'text', 1 => '/_profiler/purge',],], 4 => [], 5 => [],],
                '_profiler_info'           => [0 => [0 => 'about',], 1 => ['_controller' => 'web_profiler.controller.profiler:infoAction',], 2 => [], 3 => [0 => [0 => 'variable', 1 => '/', 2 => '[^/]++', 3 => 'about',], 1 => [0 => 'text', 1 => '/_profiler/info',],], 4 => [], 5 => [],],
                '_profiler_phpinfo'        => [0 => [], 1 => ['_controller' => 'web_profiler.controller.profiler:phpinfoAction',], 2 => [], 3 => [0 => [0 => 'text', 1 => '/_profiler/phpinfo',],], 4 => [], 5 => [],],
                '_profiler_search_results' => [0 => [0 => 'token',], 1 => ['_controller' => 'web_profiler.controller.profiler:searchResultsAction',], 2 => [], 3 => [0 => [0 => 'text', 1 => '/search/results',], 1 => [0 => 'variable', 1 => '/', 2 => '[^/]++', 3 => 'token',], 2 => [0 => 'text', 1 => '/_profiler',],], 4 => [], 5 => [],],
                '_profiler'                => [0 => [0 => 'token',], 1 => ['_controller' => 'web_profiler.controller.profiler:panelAction',], 2 => [], 3 => [0 => [0 => 'variable', 1 => '/', 2 => '[^/]++', 3 => 'token',], 1 => [0 => 'text', 1 => '/_profiler',],], 4 => [], 5 => [],],
                '_profiler_router'         => [0 => [0 => 'token',], 1 => ['_controller' => 'web_profiler.controller.router:panelAction',], 2 => [], 3 => [0 => [0 => 'text', 1 => '/router',], 1 => [0 => 'variable', 1 => '/', 2 => '[^/]++', 3 => 'token',], 2 => [0 => 'text', 1 => '/_profiler',],], 4 => [], 5 => [],],
                '_profiler_exception'      => [0 => [0 => 'token',], 1 => ['_controller' => 'web_profiler.controller.exception:showAction',], 2 => [], 3 => [0 => [0 => 'text', 1 => '/exception',], 1 => [0 => 'variable', 1 => '/', 2 => '[^/]++', 3 => 'token',], 2 => [0 => 'text', 1 => '/_profiler',],], 4 => [], 5 => [],],
                '_profiler_exception_css'  => [0 => [0 => 'token',], 1 => ['_controller' => 'web_profiler.controller.exception:cssAction',], 2 => [], 3 => [0 => [0 => 'text', 1 => '/exception.css',], 1 => [0 => 'variable', 1 => '/', 2 => '[^/]++', 3 => 'token',], 2 => [0 => 'text', 1 => '/_profiler',],], 4 => [], 5 => [],],
                '_twig_error_test'         => [0 => [0 => 'code', 1 => '_format',], 1 => ['_controller' => 'twig.controller.preview_error:previewErrorPageAction', '_format' => 'html',], 2 => ['code' => '\\d+',], 3 => [0 => [0 => 'variable', 1 => '.', 2 => '[^/]++', 3 => '_format',], 1 => [0 => 'variable', 1 => '/', 2 => '\\d+', 3 => 'code',], 2 => [0 => 'text', 1 => '/_error',],], 4 => [], 5 => [],],
                'articletagged_index'      => [0 => [], 1 => ['_controller' => 'ArticleBundle\\Controller\\ArticleTaggedController::indexAction',], 2 => ['_method' => 'GET',], 3 => [0 => [0 => 'text', 1 => '/articletagged/',],], 4 => [], 5 => [],],
                'articletagged_new'        => [0 => [], 1 => ['_controller' => 'ArticleBundle\\Controller\\ArticleTaggedController::newAction',], 2 => ['_method' => 'GET|POST',], 3 => [0 => [0 => 'text', 1 => '/articletagged/new',],], 4 => [], 5 => [],],
                'articletagged_show'       => [0 => [0 => 'url',], 1 => ['_controller' => 'ArticleBundle\\Controller\\ArticleTaggedController::showAction',], 2 => ['_method' => 'GET',], 3 => [0 => [0 => 'variable', 1 => '/', 2 => '[^/]++', 3 => 'url',], 1 => [0 => 'text', 1 => '/articletagged',],], 4 => [], 5 => [],],
                'articletagged_tag_show'   => [0 => [0 => 'tag',], 1 => ['_controller' => 'ArticleBundle\\Controller\\ArticleTaggedController::tagAction',], 2 => ['_method' => 'GET',], 3 => [0 => [0 => 'variable', 1 => '/', 2 => '[^/]++', 3 => 'tag',], 1 => [0 => 'text', 1 => '/articletagged/tag',],], 4 => [], 5 => [],],
                'articletagged_edit'       => [0 => [0 => 'id',], 1 => ['_controller' => 'ArticleBundle\\Controller\\ArticleTaggedController::editAction',], 2 => ['_method' => 'GET|POST',], 3 => [0 => [0 => 'text', 1 => '/edit',], 1 => [0 => 'variable', 1 => '/', 2 => '[^/]++', 3 => 'id',], 2 => [0 => 'text', 1 => '/articletagged',],], 4 => [], 5 => [],],
                'articletagged_delete'     => [0 => [0 => 'id',], 1 => ['_controller' => 'ArticleBundle\\Controller\\ArticleTaggedController::deleteAction',], 2 => ['_method' => 'DELETE',], 3 => [0 => [0 => 'variable', 1 => '/', 2 => '[^/]++', 3 => 'id',], 1 => [0 => 'text', 1 => '/articletagged',],], 4 => [], 5 => [],],
                'articleauthors_index'     => [0 => [], 1 => ['_controller' => 'ArticleBundle\\Controller\\ArticleAuthorsController::indexAction',], 2 => ['_method' => 'GET',], 3 => [0 => [0 => 'text', 1 => '/articleauthors/',],], 4 => [], 5 => [],],
                'articleauthors_new'       => [0 => [], 1 => ['_controller' => 'ArticleBundle\\Controller\\ArticleAuthorsController::newAction',], 2 => ['_method' => 'GET|POST',], 3 => [0 => [0 => 'text', 1 => '/articleauthors/new',],], 4 => [], 5 => [],],
                'articleauthors_show'      => [0 => [0 => 'url',], 1 => ['_controller' => 'ArticleBundle\\Controller\\ArticleAuthorsController::showAction',], 2 => ['_method' => 'GET',], 3 => [0 => [0 => 'variable', 1 => '/', 2 => '[^/]++', 3 => 'url',], 1 => [0 => 'text', 1 => '/articleauthors',],], 4 => [], 5 => [],],
                'articleauthors_edit'      => [0 => [0 => 'id',], 1 => ['_controller' => 'ArticleBundle\\Controller\\ArticleAuthorsController::editAction',], 2 => ['_method' => 'GET|POST',], 3 => [0 => [0 => 'text', 1 => '/edit',], 1 => [0 => 'variable', 1 => '/', 2 => '[^/]++', 3 => 'id',], 2 => [0 => 'text', 1 => '/articleauthors',],], 4 => [], 5 => [],],
                'articleauthors_delete'    => [0 => [0 => 'id',], 1 => ['_controller' => 'ArticleBundle\\Controller\\ArticleAuthorsController::deleteAction',], 2 => ['_method' => 'DELETE',], 3 => [0 => [0 => 'variable', 1 => '/', 2 => '[^/]++', 3 => 'id',], 1 => [0 => 'text', 1 => '/articleauthors',],], 4 => [], 5 => [],],
                'article_index'            => [0 => [], 1 => ['_controller' => 'ArticleBundle\\Controller\\ArticleController::indexAction',], 2 => ['_method' => 'GET',], 3 => [0 => [0 => 'text', 1 => '/',],], 4 => [], 5 => [],],
            ];
        }
    }

    public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_PATH)
    {
        if (!isset(self::$declaredRoutes[$name])) {
            throw new RouteNotFoundException(sprintf('Unable to generate a URL for the named route "%s" as such route does not exist.', $name));
        }

        list($variables, $defaults, $requirements, $tokens, $hostTokens, $requiredSchemes) = self::$declaredRoutes[$name];

        return $this->doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }
}
