<?php declare(strict_types=1);

namespace ArticleBundle\Transformer;

use ArticleBundle\Entity\Tag;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class TagTransformer implements DataTransformerInterface
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * Преобразует коллекцию в список через запятую
     *
     * @param mixed $value The value in the original representation
     *
     * @return mixed The value in the transformed representation
     *
     * @throws TransformationFailedException When the transformation fails.
     */
    public function transform($value)
    {
        $array = $value->toArray();
        $resultArray = [];
        array_walk($array, function ($element) use (&$resultArray) {
            $resultArray[] = $element->getName();
        });

        return implode(', ', $resultArray);
    }

    /**
     * Преобразует строку в массив элементов
     *
     * @param mixed $value The value in the transformed representation
     *
     * @return mixed The value in the original representation
     *
     * @throws TransformationFailedException When the transformation fails.
     */
    public function reverseTransform($value)
    {
        $array = explode(', ', $value);
        $resultArray = [];
        $slice = array_slice($array, 0, 3);
        array_walk($slice, function ($tag) use (&$resultArray) {
            $resultArray[] = $this->em->getRepository(Tag::class)->findOrCreate($tag);
        });

        return $resultArray;
    }
}