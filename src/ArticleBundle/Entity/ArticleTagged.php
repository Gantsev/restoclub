<?php
/**
 * Created by PhpStorm.
 * User: gantsev
 * Date: 17.09.17
 * Time: 16:09
 */

namespace ArticleBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="article_tagged")
 */
class ArticleTagged extends Article
{
    /**
     * Many Articles have Many tags.
     *
     * @ORM\ManyToMany(targetEntity="Tag")
     * @ORM\JoinTable(name="article_tags",
     *      joinColumns={@ORM\JoinColumn(name="article_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     *      )
     */
    protected $tags;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    /**
     * Add tag
     *
     * @param \ArticleBundle\Entity\Tag $tag
     *
     * @return ArticleTagged
     */
    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \ArticleBundle\Entity\Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     * @access
     */
    public function getTags()
    {
        return $this->tags;
    }
}
