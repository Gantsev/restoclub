<?php
/**
 * Created by PhpStorm.
 * User: gantsev
 * Date: 17.09.17
 * Time: 16:09
 */

namespace ArticleBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="article_authors")
 */
class ArticleAuthors extends Article
{
    /**
     * @ORM\Column(type="string")
     */
    protected $author;

    /**
     * @ORM\Column(type="string")
     */
    protected $site;

    /**
     * Set author
     *
     * @param string $author
     *
     * @return ArticleAuthors
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set site
     *
     * @param string $site
     *
     * @return ArticleAuthors
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }
}
