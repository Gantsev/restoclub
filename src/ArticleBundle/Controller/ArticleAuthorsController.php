<?php

namespace ArticleBundle\Controller;

use ArticleBundle\Entity\ArticleAuthors;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Авторские статьи
 *
 * @Route("articleauthors")
 */
class ArticleAuthorsController extends Controller
{
    /**
     * Lists all articleAuthor entities.
     *
     * @Route("/", name="articleauthors_index")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @access
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $articleAuthors = $em->getRepository('ArticleBundle:ArticleAuthors')->findAll();

        return $this->render('articleauthors/index.html.twig', [
            'articleAuthors' => $articleAuthors,
        ]);
    }

    /**
     * Creates a new articleAuthor entity.
     *
     * @Route("/new", name="articleauthors_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @access
     */
    public function newAction(Request $request)
    {
        $articleAuthor = new ArticleAuthors();
        $form = $this->createForm('ArticleBundle\Form\ArticleAuthorsType', $articleAuthor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($articleAuthor);
            $em->flush();

            return $this->redirectToRoute('articleauthors_show', ['url' => $articleAuthor->getUrl()]);
        }

        return $this->render('articleauthors/new.html.twig', [
            'articleAuthor' => $articleAuthor,
            'form'          => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a articleAuthor entity.
     *
     * @Route("/{url}", name="articleauthors_show")
     * @Method("GET")
     *
     * @param ArticleAuthors $articleAuthor
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @access
     */
    public function showAction(ArticleAuthors $articleAuthor)
    {
        $deleteForm = $this->createDeleteForm($articleAuthor);

        return $this->render('articleauthors/show.html.twig', [
            'articleAuthor' => $articleAuthor,
            'delete_form'   => $deleteForm->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing articleAuthor entity.
     *
     * @Route("/{id}/edit", name="articleauthors_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param ArticleAuthors $articleAuthor
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @access
     */
    public function editAction(Request $request, ArticleAuthors $articleAuthor)
    {
        $deleteForm = $this->createDeleteForm($articleAuthor);
        $editForm = $this->createForm('ArticleBundle\Form\ArticleAuthorsType', $articleAuthor);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('articleauthors_edit', ['id' => $articleAuthor->getId()]);
        }

        return $this->render('articleauthors/edit.html.twig', [
            'articleAuthor' => $articleAuthor,
            'edit_form'     => $editForm->createView(),
            'delete_form'   => $deleteForm->createView(),
        ]);
    }

    /**
     * Deletes a articleAuthor entity.
     *
     * @Route("/{id}", name="articleauthors_delete")
     * @Method("DELETE")
     *
     * @param Request $request
     * @param ArticleAuthors $articleAuthor
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @access
     */
    public function deleteAction(Request $request, ArticleAuthors $articleAuthor)
    {
        $form = $this->createDeleteForm($articleAuthor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($articleAuthor);
            $em->flush();
        }

        return $this->redirectToRoute('articleauthors_index');
    }

    /**
     * Creates a form to delete a articleAuthor entity.
     *
     * @param ArticleAuthors $articleAuthor The articleAuthor entity
     *
     * @param ArticleAuthors $articleAuthor
     *
     * @return \Symfony\Component\Form\Form|\Symfony\Component\Form\FormInterface
     * @access
     */
    private function createDeleteForm(ArticleAuthors $articleAuthor)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('articleauthors_delete', ['id' => $articleAuthor->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
