<?php

namespace ArticleBundle\Controller;

use ArticleBundle\Entity\ArticleTagged;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Статьи с тэгами
 *
 * @Route("articletagged")
 */
class ArticleTaggedController extends Controller
{
    /**
     * Lists all articleTagged entities.
     *
     * @Route("/", name="articletagged_index")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @access
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $articleTaggeds = $em->getRepository('ArticleBundle:ArticleTagged')->findAll();

        return $this->render('articletagged/index.html.twig', [
            'articleTaggeds' => $articleTaggeds,
        ]);
    }

    /**
     * Creates a new articleTagged entity.
     *
     * @Route("/new", name="articletagged_new")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @access
     */
    public function newAction(Request $request)
    {
        $articleTagged = new Articletagged();
        $form = $this->createForm('ArticleBundle\Form\ArticleTaggedType', $articleTagged);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($articleTagged);
            $em->flush();

            return $this->redirectToRoute('articletagged_show', ['url' => $articleTagged->getUrl()]);
        }

        return $this->render('articletagged/new.html.twig', [
            'articleTagged' => $articleTagged,
            'form'          => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a articleTagged entity.
     *
     * @Route("/{url}", name="articletagged_show")
     * @Method("GET")
     *
     * @param ArticleTagged $articleTagged
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @access
     */
    public function showAction(ArticleTagged $articleTagged)
    {
        $deleteForm = $this->createDeleteForm($articleTagged);

        return $this->render('articletagged/show.html.twig', [
            'articleTagged' => $articleTagged,
            'delete_form'   => $deleteForm->createView(),
        ]);
    }

    /**
     * Finds and displays a articleTagged entity.
     *
     * @Route("/tag/{tag}", name="articletagged_tag_show")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @access
     */
    public function tagAction($tag)
    {
        $em = $this->getDoctrine()->getManager();

        $tags = $em->getRepository('ArticleBundle:Tag')->findOneBy(['name' => $tag]);

        return $this->render('articletagged/show_tag.html.twig', [
            'articles' => $tags->getArticles(),
        ]);
    }

    /**
     * Displays a form to edit an existing articleTagged entity.
     *
     * @Route("/{id}/edit", name="articletagged_edit")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     * @param ArticleTagged $articleTagged
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @access
     */
    public function editAction(Request $request, ArticleTagged $articleTagged)
    {
        $deleteForm = $this->createDeleteForm($articleTagged);
        $editForm = $this->createForm('ArticleBundle\Form\ArticleTaggedType', $articleTagged);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('articletagged_edit', ['id' => $articleTagged->getId()]);
        }

        return $this->render('articletagged/edit.html.twig', [
            'articleTagged' => $articleTagged,
            'edit_form'     => $editForm->createView(),
            'delete_form'   => $deleteForm->createView(),
        ]);
    }

    /**
     * Deletes a articleTagged entity.
     *
     * @Route("/{id}", name="articletagged_delete")
     * @Method("DELETE")
     *
     * @param Request $request
     * @param ArticleTagged $articleTagged
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, ArticleTagged $articleTagged)
    {
        $form = $this->createDeleteForm($articleTagged);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($articleTagged);
            $em->flush();
        }

        return $this->redirectToRoute('articletagged_index');
    }

    /**
     * Creates a form to delete a articleTagged entity.
     *
     * @param ArticleTagged $articleTagged The articleTagged entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ArticleTagged $articleTagged)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('articletagged_delete', ['id' => $articleTagged->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
