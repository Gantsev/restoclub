<?php

namespace ArticleBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * ArticleController controller.
 *
 * @Route("/")
 */
class ArticleController extends Controller
{
    /**
     *
     * Показ возможных статей
     *
     * @Route("/", name="article_index")
     * @Method("GET")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @access
     */
    public function indexAction()
    {
        return $this->render('article/index.html.twig');
    }
}
